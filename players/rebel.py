#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
from random import randint, random

N_PLAYERS = 3
N_MAX = 5

if __name__ == '__main__':
    if randint(0, 1) == 0:
        guess = randint(-10, N_PLAYERS*N_MAX)
        draw = randint(-4, min(guess, N_MAX))  # Will raise if guess < -4
        sys.stdout.write("%d %d" % (guess, draw))
    else:
        sys.stdout.write("I'm rebel!")

