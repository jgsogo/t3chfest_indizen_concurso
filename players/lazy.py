#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
from random import randint, random

N_PLAYERS = 3
N_MAX = 5

if __name__ == '__main__':
    guess = randint(0, N_PLAYERS*N_MAX)
    draw = randint(0, min(guess, N_MAX))
    time.sleep(random()*1.5)
    sys.stdout.write("%d %d" % (guess, draw))

