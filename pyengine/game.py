#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
import operator
from player import InvalidOutput, build_player


N_PLAYERS = 3
N_MAX = 5
N_ROUNDS = 100
TIMEOUT_SECONDS = 1




class Game:
    def __init__(self, players):
        self.players = players
        # Create temporal files to store guesses and draws
        base_dir = os.path.join(os.path.dirname(__file__), 'tmp')
        if not os.path.exists(base_dir):
            os.makedirs(base_dir)
        base_name = str(uuid.uuid4())
        self.guesses = os.path.join(base_dir, base_name + '_g.txt')
        self.draws = os.path.join(base_dir, base_name + '_d.txt')
        # Initialize variables
        for p in self.players:
            p.guesses_file = self.guesses
            p.draws_file = self.draws
        self.wins = [0]*len(self.players)
        self.elapsed = [0]*len(self.players)

    @classmethod
    def find_winners(cls, guesses, x):
        return [i for i, e in enumerate(guesses) if e==x]

    def play(self, rounds=N_ROUNDS, timeout_seconds=TIMEOUT_SECONDS, delete_files=True):
        try:
            # Ensure files are already created:
            open(self.guesses, 'a').close()
            open(self.draws, 'a').close()
            # Play rounds
            for i in range(1, rounds+1):
                print("\nRound %d/%d:" % (i, rounds))
                guesses, draws, round_elapsed = Game.round(self.players, timeout_seconds)

                # Write to file
                with open(self.guesses, 'a') as f:
                    f.write("%s\n" % ' '.join(["%d" % it if it else '-1' for it in guesses]))
                with open(self.draws, 'a') as f:
                    f.write("%s\n" % ' '.join(["%d" % it if it else '-1' for it in draws]))

                # Evaluate
                s = sum(filter(None, draws))
                winners = Game.find_winners(guesses, s)
                if len(winners):
                    for w in winners:
                        self.wins[w] += 1
                    print("Winners: %s" % winners)
                else:
                    print("No one wins")

                # Append times
                self.elapsed = map(operator.add, self.elapsed, round_elapsed)

        except KeyboardInterrupt:
            pass

        # Delete files
        if delete_files:
            os.remove(self.guesses)
            os.remove(self.draws)

        return self.wins, list(self.elapsed)

    @classmethod
    def round(cls, players, timeout_seconds):
        guesses = []
        draws = []
        round_elapsed = []
        # Round
        for p, j in zip(players, range(len(players))):
            try:
                guess, draw, elapsed = p.run(guesses, timeout_seconds=timeout_seconds)
                print(" - %s: %d %d" % ('player %d' % j, guess, draw))
            except InvalidOutput as e:
                print(" - %s: %s" % ('player %d' % j, str(e)))
                guess = None
                draw = None
                elapsed = timeout_seconds
            guesses.append(guess)
            draws.append(draw)
            round_elapsed.append(elapsed)

        return guesses, draws, round_elapsed


if __name__ == '__main__':
    print("=" * 20)
    print("T3chFest - Indizen")
    print("\n== Concurso de programación")

    # Get path to players and build player instances
    import os
    players = [os.path.join(os.path.dirname(__file__), '../players', 'dummy.py'),
               os.path.join(os.path.dirname(__file__), '../players', 'lazy.py'),
               os.path.join(os.path.dirname(__file__), '../players', 'rebel.py')]

    players = [build_player(p, type='python', max_draw=N_MAX) for p in players]

    for p, i in zip(players, range(len(players)+1)):
        print(" - player %d: %s" % (i, p))

    # Match!
    game = Game(players)
    wins, times = game.play()

    # Dump results
    print("\n===== Match ended!")
    print("Rounds won by each player: %s" % wins)
    print("Time consumed by each player: %s" % times)
