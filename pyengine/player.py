#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
import datetime as dt



class InvalidOutput(Exception):
    pass


class BaseCommand(object):
    def __init__(self, cmd):
        self.cmd = cmd
    
    def communicate(self, timeout_seconds):
        pass

if sys.version_info < (3,0):

    import threading

    class Command(BaseCommand):
        def __init__(self, *args, **kwargs):
            super(Command, self).__init__(*args, **kwargs)
            self.data = None
            self.process = None

        def target(self):
            self.process = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            self.data, err = self.process.communicate()

        def communicate(self, timeout_seconds):
            thread = threading.Thread(target=self.target)
            thread.start()

            thread.join(timeout_seconds)
            if thread.is_alive():
                self.process.terminate()
                thread.join()
                raise InvalidOutput("Timeout!")
            return self.data

else:

    class Command(BaseCommand):
        def communicate(self, timeout_seconds):
            try:
                p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = p.communicate(timeout=timeout_seconds)
                return out
            except subprocess.TimeoutExpired:
                raise InvalidOutput("Timeout!")
    

class ProxyPlayer:
    def __init__(self, exe, max_draw):
        self._exe = exe
        self.guesses_file = None
        self.draws_file = None
        self.MAX_DRAW = max_draw

    def __str__(self):
        return str(self._exe)

    def run(self, guesses, timeout_seconds):
        command = self._get_command() + [self.guesses_file, self.draws_file] + [str(g) if g else '-1' for g in guesses]
        before = dt.datetime.now()
        out = Command(command).communicate(timeout_seconds)
        after = dt.datetime.now()
        g, d = ProxyPlayer._return(out, self.MAX_DRAW)
        return g, d, ProxyPlayer._elapsed(before, after)

    def _get_command(self):
        raise NotImplementedError("Instantiate with player type: C++, Python,...")

    @classmethod
    def _elapsed(cls, before, after):
        return (after-before).microseconds / 10e6

    @classmethod
    def _return(cls, output, max_draw):
        try:
            guess, draw = (int(a) for a in output.split())
            if draw > max_draw or draw < 0:
                raise InvalidOutput("Invalid draw: %d" % draw)
            return guess, draw
        except ValueError as e:
            raise InvalidOutput("Invalid output: %s" % (output))


# Some specialization for players depending on languages
class PythonProxyPlayer(ProxyPlayer):
    def _get_command(self):
        return ['python', self._exe]


class ShellPlayer(ProxyPlayer):
    def _get_command(self):
        return [self._exe]


def build_player(exe, max_draw, type=None):
    if type == 'python':
        return PythonProxyPlayer(exe, max_draw)
    elif type == 'shell':
        return ShellPlayer(exe, max_draw)
    else:
        raise NotImplementedError("TODO: Try to guess program type based on file extension")
